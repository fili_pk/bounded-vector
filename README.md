# bounded-vector

[![crates.io](https://img.shields.io/crates/v/bounded-vector.svg)](https://crates.io/crates/bounded-vector)

`BoundedVec<T, LOW, UPP>` - Vec wrapper that guarantees upper and lower bounds on type level.
Alternative to [bounded-vec](https://github.com/ergoplatform/bounded-vec) that offers compatibility with empty vector, has more methods and `bvec!` that works like vec! macro.

## Example
```rust
use bounded_vector::{BoundedVec, bvec};
let mut data: BoundedVec<u8, 2, 4> = [1, 2].try_into()?;

assert_eq!(data.first(), Some(1));
assert_eq!(data.last(), Some(2));

data.iter_mut().for_each(|x| *x *= 2);
assert_eq!(data, bvec![2, 4]?);
```
## License
Licensed under either of [MIT](LICENSE-MIT) or [Apache-2.0](LICENSE-APACHE) at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in this crate by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions. 
