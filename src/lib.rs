//! [`BoundedVec<T, LOW, UPP>`] - Vec wrapper that guarantees upper and lower bounds on type level.
//! Alternative to [bounded-vec](https://github.com/ergoplatform/bounded-vec) that offers compatibility with empty vector, has more methods and [`bvec!`] that works like vec! macro.
//!
//! Example
//! ```rust
//! # use std::error::Error;
//! #
//! # fn main() -> Result<(), Box<dyn Error>> {
//! use bounded_vector::{BoundedVec, bvec};
//! let mut data: BoundedVec<u8, 2, 4> = [1, 2].try_into()?;
//!
//! assert_eq!(data.first(), Some(&1));
//! assert_eq!(data.last(), Some(&2));
//!
//! data.iter_mut().for_each(|x| *x *= 2);
//! assert_eq!(data, bvec![2, 4]?);
//! #     Ok(())
//! # }
//! ```

mod bounded_vec;
mod macros;
pub use bounded_vec::{BoundedVec, Error};
