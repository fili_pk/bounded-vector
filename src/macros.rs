/// Macro similar to vec!
///
///
/// Examples
///
/// Creating BoundedVec from elements
/// ```rust
/// # use std::error::Error;
/// # use bounded_vector::{BoundedVec, bvec};
/// #
/// # fn main() -> Result<(), Box<dyn Error>> {
/// let bvec: BoundedVec<i32, 0, 3> = bvec![1, 2, 3]?;
/// assert_eq!(vec![1,2,3], bvec.to_vec());
/// # Ok(())
/// # }
/// ```
///
/// Creating BoundedVec from n same elements(must have clone trait)
/// ```rust
/// # use std::error::Error;
/// # use bounded_vector::{BoundedVec, bvec};
/// #
/// # fn main() -> Result<(), Box<dyn Error>> {
/// let bvec: BoundedVec<i32, 0, 3> = bvec![1; 3]?;
/// assert_eq!(vec![1,1,1], bvec.to_vec());
/// # Ok(())
/// # }
/// ```
///
/// Macro gives Error for first two usages if element count don't fit bounds
/// ```rust
/// # use std::error;
/// # use bounded_vector::{BoundedVec, bvec, Error};
/// #
/// # fn main() -> Result<(), Box<dyn error::Error>> {
/// let bvec: Result<BoundedVec<i32, 0, 0>, _> = bvec![1];
/// assert_eq!(bvec, Err(Error::OutOfBoundsVec));
/// # Ok(())
/// # }
/// ```
///
/// Creating empty BoundedVec<_, 0, _>. This usage does not return Result because it ensures correct
/// bounds at the compile time
/// ```rust
/// # use std::error::Error;
/// # use bounded_vector::{BoundedVec, bvec};
/// #
/// # fn main() -> Result<(), Box<dyn Error>> {
/// let bvec: BoundedVec<i32, 0, 0> = bvec![];
/// assert_eq!(BoundedVec::new(), bvec);
/// # Ok(())
/// # }
/// ```
#[macro_export]
macro_rules! bvec {
	() => {{
		$crate::BoundedVec::new()
	}};

	($elem:expr; $n:expr) => {
		$crate::BoundedVec::from_elem($elem, $n)
	};

	($($x:expr),+ $(,)?) => {
        $crate::BoundedVec::try_from(<[_]>::into_vec(Box::new([$($x),+])))
	};
}

#[cfg(test)]
mod tests {
	use crate::*;

	#[test]
	fn empty() {
		let vec_from_macro: BoundedVec<i32, 0, 0> = bvec![];
		let vec = BoundedVec::<i32, 0, 0>::new();
		assert_eq!(vec_from_macro, vec);
	}

	#[test]
	fn from_elements() {
		let vec_from_macro: BoundedVec<_, 3, 3> = bvec![1, 2, 3].unwrap();
		assert_eq!(vec_from_macro.to_vec(), vec![1, 2, 3]);

		let vec_from_macro: Result<BoundedVec<_, 0, 0>, _> = bvec![1, 2, 3];
		assert_eq!(vec_from_macro, Err(Error::OutOfBoundsVec));
	}

	#[test]
	fn n_elements() {
		let vec_from_macro: BoundedVec<_, 3, 3> = bvec![2u8; 3].unwrap();
		assert_eq!(vec_from_macro.to_vec(), vec![2, 2, 2]);

		let vec_from_macro: Result<BoundedVec<_, 0, 0>, _> = bvec![2u8; 3];
		assert_eq!(vec_from_macro, Err(Error::OutOfBoundsVec));
	}
}
